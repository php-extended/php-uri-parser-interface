<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use PhpExtended\Parser\ParserInterface;
use Psr\Http\Message\UriInterface;

/**
 * UriParserInterface interface file.
 * 
 * This parser parses unified resource identifiers (URIs).
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<UriInterface>
 */
interface UriParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
